<?php 

require_once(dirname( __FILE__ ).'/../../../../wp-config.php');
require_once(dirname( __FILE__ ).'/../../../../wp-load.php');

class Tracking{

	public function getBikes($name = '', $email = ''){
		global $wpdb;
		$user_id = get_current_user_id();

		if( !($user_id > 0)){
			return false;
		}
		

		if($name == '' && $email == ''){
			$results = $wpdb->get_results( $wpdb->prepare( "SELECT *  FROM wp_tracking_bikes, wp_users u WHERE wp_tracking_bikes.user_id = u.ID ORDER BY wp_tracking_bikes.id DESC "), OBJECT );
		}
		if($name == '' && $email != ''){
			$results = $wpdb->get_results( $wpdb->prepare( "SELECT *  FROM wp_tracking_bikes, wp_users u WHERE wp_tracking_bikes.user_id = u.ID and u.user_email like '%s' ", "%$email%"), OBJECT );
		}
		if($name != '' && $email == ''){
			$results = $wpdb->get_results( $wpdb->prepare( "SELECT *  FROM wp_tracking_bikes, wp_users u WHERE wp_tracking_bikes.user_id = u.ID and u.display_name like '%s' ORDER BY wp_tracking_bikes.id DESC ", "%$name%"), OBJECT );

		}
		if($name != '' && $email != ''){
			$results = $wpdb->get_results( $wpdb->prepare( "SELECT *  FROM wp_tracking_bikes, wp_users u WHERE wp_tracking_bikes.user_id = u.ID and u.display_name like '%s' and u.user_email like '%s' ORDER BY wp_tracking_bikes.id DESC ", "%$name%", "%$email%"), OBJECT );
		}
		

		return $results;		
	}

	public function getBikeServices($id){
		global $wpdb;
		$user_id = get_current_user_id();

		if( !($user_id > 0)){
			return false;
		}
		

		$results = $wpdb->get_results( $wpdb->prepare( "SELECT *  FROM wp_tracking_services WHERE bike_id = '%s' ORDER BY id DESC ", $id), OBJECT );
			

		return $results;		
	}

	public function getService($id){
		global $wpdb;
		$user_id = get_current_user_id();

		if( !($user_id > 0)){
			return false;
		}
		

		$results = $wpdb->get_results( $wpdb->prepare( "SELECT *  FROM wp_tracking_services WHERE id = '%s' ", $id), OBJECT );
			

		return $results;		
	}

	public function getBike($id){
		global $wpdb;
		$user_id = get_current_user_id();

		if(empty($id)){
			$messages[] = "- El id es obligatorio \n";
		}

		$user_id = get_current_user_id();
		if( !($user_id > 0)){
			$messages[] = "- El usuario es obligatorio \n";
		}

		if(count($messages) > 0){
			return array('status' => 'ER', 'messages' => $messages);
		}

		$results = $wpdb->get_results( $wpdb->prepare( "SELECT *  FROM wp_tracking_bikes WHERE  `id` = '%s' ORDER BY `id` DESC ", $id), OBJECT );
		return $results;		
	}


	public function insert_bike($serie, $marca, $modalidad, $rin, $material, $grupo, $frenos, $suspension){

		
		$messages = [];
		if(empty($serie)){
			$messages[] = "- La serie es obligatoria \n";
		}
		if(empty($marca)){
			$messages[] = "- La marca es obligatoria \n";
		}
		if(empty($modalidad)){
			$messages[] = "- La modalidad es obligatoria \n";
		}
		if(empty($rin)){
			$messages[] = "- El rin es obligatorio \n";
		}
		if(empty($material)){
			$messages[] = "- El material es obligatorio \n";
		}
		if(empty($grupo)){
			$messages[] = "- El grupo es obligatorio \n";
		}
		if(empty($frenos)){
			$messages[] = "- El freno es obligatoria \n";
		}
		if(empty($suspension)){
			$messages[] = "- La suspension es obligatoria \n";
		}

		$user_id = get_current_user_id();
		if( !($user_id > 0)){
			$messages[] = "- El usuario es obligatorio \n";
		}

		if(count($messages) > 0){
			return array('status' => 'ER', 'messages' => $messages);
		}


		global $wpdb;
		$result = $wpdb->insert('wp_tracking_bikes', array('serie' => $serie,
													'marca' => $marca,
													'modalidad' => $modalidad,
													'tam_rin' => $rin,
													'material' => $material,
													'grupo' => $grupo,
													'frenos' => $frenos,
													'suspension' => $suspension,
													'user_id' => $user_id));
		if(!$result){
			
			return array('status' => 'ER', 'message'=>'no se ha podido crear la bicicleta', 'error' => $wpdb->last_error);
		}
		return array('status' => 'OK', 'message' => 'La bicicleta ha sido creada con exito');
	}

	public function update_bike($id, $serie, $marca, $modalidad, $rin, $material, $grupo, $frenos, $suspension){

		
		$messages = [];
		if(empty($id)){
			$messages[] = "- El id es obligatorio \n";
		}
		if(empty($serie)){
			$messages[] = "- La serie es obligatoria \n";
		}
		if(empty($marca)){
			$messages[] = "- La marca es obligatoria \n";
		}
		if(empty($modalidad)){
			$messages[] = "- La modalidad es obligatoria \n";
		}
		if(empty($rin)){
			$messages[] = "- El rin es obligatorio \n";
		}
		if(empty($material)){
			$messages[] = "- El material es obligatorio \n";
		}
		if(empty($grupo)){
			$messages[] = "- El grupo es obligatorio \n";
		}
		if(empty($frenos)){
			$messages[] = "- El freno es obligatoria \n";
		}
		if(empty($suspension)){
			$messages[] = "- La suspension es obligatoria \n";
		}

		$user_id = get_current_user_id();
		if( !($user_id > 0)){
			$messages[] = "- El usuario es obligatorio \n";
		}

		if(count($messages) > 0){
			return array('status' => 'ER', 'messages' => $messages);
		}


		global $wpdb;
		$result = $wpdb->update('wp_tracking_bikes', array('serie' => $serie,
													'marca' => $marca,
													'modalidad' => $modalidad,
													'tam_rin' => $rin,
													'material' => $material,
													'grupo' => $grupo,
													'frenos' => $frenos,
													'suspension' => $suspension,
													'user_id' => $user_id),
												array('id' => $id));
		if(!$result){
			
			return array('status' => 'ER', 'message'=>'no se ha podido actualizar la bicicleta', 'error' => $wpdb->last_error);
		}
		return array('status' => 'OK', 'message' => 'La bicicleta ha sido actulizada con exito');
	}

	public function deleteService($id){
		global $wpdb;
		$user_id = get_current_user_id();

		if(empty($id)){
			$messages[] = "- El id es obligatorio \n";
		}

		$user_id = get_current_user_id();
		if( !($user_id > 0)){
			$messages[] = "- El usuario es obligatorio \n";
		}

		if(count($messages) > 0){
			return array('status' => 'ER', 'messages' => $messages);
		}

		$result = $wpdb->delete('wp_tracking_services',array('id' => $id));
		if(!$result){
			
			return array('status' => 'ER', 'message'=>'no se ha podido eliminar el servicio', 'error' => $wpdb->last_error);
		}
		return array('status' => 'OK', 'message' => 'El servicio ha sido eliminado con exito');	
	}

	public function get_all_parameters(){
		global $wpdb;
		$results = $wpdb->get_results( 'SELECT * FROM wp_tracking_fields ', OBJECT );
		//$mylink = $result = $wpdb->query('SELECT * FROM wp_rtb_place ');
		return $results;		
	}

	public function insert_parameter($parameter, $reg){
		
		global $wpdb;
		$result = $wpdb->insert('wp_tracking_fields', array('name' => $parameter['name'], 'key'=>$parameter['key'], 'value'=>$parameter['value']));
		if(!$result){
			return array('status' => 'ER', 'message'=>'no se ha podido crear el parametro', 'key'=>$parameter['key'], 'reg' => $reg);
		}
		return array('status' => 'OK');
	}

	public function delete_all_paramteres(){
		global $wpdb;
		$delete = $wpdb->query("TRUNCATE TABLE `wp_tracking_fields`");
	}

	public function getParameter($key){
		global $wpdb;

		$results = $wpdb->get_results( $wpdb->prepare( "SELECT `value` FROM wp_tracking_fields 
					WHERE  `key` = '%s' ", $key ), OBJECT );			
		
		if(count($results) == 0){
			$results = array("status" => 'ER', 'message' => 'no se pudo recueperar el parametro' );
		}
		return $results[0]->value;
	}

	public function insert_service($fecha, $clase, $observaciones, $fotos_id, $fotos_url, $bike_id){

		
		$messages = [];
		if(empty($fecha)){
			$messages[] = "- La fecha es obligatoria \n";
		}
		if(empty($clase)){
			$messages[] = "- La clase es obligatoria \n";
		}
		if(empty($observaciones)){
			$messages[] = "- Las observaciones son obligatorias \n";
		}
		if(empty($fotos_id)){
			$messages[] = "- Las fotos son obligatorias \n";
		}elseif(empty($fotos_url)){
			$messages[] = "- Las fotos son obligatorias \n";
		}
		if(empty($bike_id)){
			$messages[] = "- El id de la bicicleta es obligatorio \n";
		}
		
		$user_id = get_current_user_id();
		if( !($user_id > 0)){
			$messages[] = "- El usuario es obligatorio \n";
		}

		if(count($messages) > 0){
			return array('status' => 'ER', 'messages' => $messages);
		}


		global $wpdb;
		$result = $wpdb->insert('wp_tracking_services', array('fecha' => $fecha,
													'clase_servicio' => $clase,
													'observaciones' => $observaciones,
													'fotos_id' => $fotos_id,
													'fotos_url' => $fotos_url,
													'bike_id' => $bike_id));
		if(!$result){
			
			return array('status' => 'ER', 'message'=>'no se ha podido crear el servicio', 'error' => $wpdb->last_error);
		}
		return array('status' => 'OK', 'message' => 'El servicio ha sido creado con exito');
	}

	public function update_service($id, $fecha, $clase, $observaciones, $fotos_id, $fotos_url, $bike_id){

		
		$messages = [];
		if(empty($id)){
			$messages[] = "- El id es obligatorio \n";
		}
		if(empty($fecha)){
			$messages[] = "- La fecha es obligatoria \n";
		}
		if(empty($clase)){
			$messages[] = "- La clase es obligatoria \n";
		}
		if(empty($observaciones)){
			$messages[] = "- Las observaciones son obligatorias \n";
		}
		if(empty($fotos_id)){
			$messages[] = "- Las fotos son obligatorias \n";
		}elseif(empty($fotos_url)){
			$messages[] = "- Las fotos son obligatorias \n";
		}
		if(empty($bike_id)){
			$messages[] = "- El id de la bicicleta es obligatorio \n";
		}
		
		$user_id = get_current_user_id();
		if( !($user_id > 0)){
			$messages[] = "- El usuario es obligatorio \n";
		}

		if(count($messages) > 0){
			return array('status' => 'ER', 'messages' => $messages);
		}

		$user_id = get_current_user_id();
		if( !($user_id > 0)){
			$messages[] = "- El usuario es obligatorio \n";
		}

		if(count($messages) > 0){
			return array('status' => 'ER', 'messages' => $messages);
		}


		global $wpdb;
		$result = $wpdb->update('wp_tracking_services', array('fecha' => $fecha,
													'clase_servicio' => $clase,
													'observaciones' => $observaciones,
													'fotos_id' => $fotos_id,
													'fotos_url' => $fotos_url),
												array('id' => $id));
		if(!$result){
			
			return array('status' => 'ER', 'message'=>'no se ha podido actualizar el servicio', 'error' => $wpdb->last_error);
		}
		return array('status' => 'OK', 'message' => 'El servicio ha sido actulizado con exito');
	}

}

?>