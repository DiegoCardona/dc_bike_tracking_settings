<?php

//echo dirname( __FILE__ );

require_once(dirname( __FILE__ ).'/../../../wp-config.php');
require_once(dirname( __FILE__ ).'/../../../wp-load.php');


/*
* Plugin Name: dc_bike_tracking_back
* Description: Funcionalidad para realizar la administracion de tracking y servicio
* Version: 1.0
* Author: DiegoCardona
* Autor Uri: http://thedeveloper.co
*/

/* helpers */
function dc_bike_tracking_back_get_url(){
	$plugin_dir = plugin_dir_path( __FILE__ );
	return plugin_dir_path( __FILE__ );
}

function dc_bike_tracking_back_js_include()
{
    wp_enqueue_script('jquery');
    wp_register_style( 'bootstrap.css', WP_PLUGIN_URL. '/dc_bike_tracking_back/css/bootstrap.min.css' );
    wp_enqueue_style( 'bootstrap.css' );
    wp_register_style( 'jquery-ui.css', WP_PLUGIN_URL. '/dc_bike_tracking_back/css/jquery-ui.css' );
    wp_enqueue_style( 'jquery-ui.css' );
    wp_register_style( 'dc_bike_tracking_back.css', WP_PLUGIN_URL. '/dc_bike_tracking_back/css/plugin.css' );
    wp_enqueue_style( 'dc_bike_tracking_back.css' );
    //wp_enqueue_script('jquery.js', WP_PLUGIN_URL. '/dc_bike_tracking_back/js/jquery.min.js' );
    wp_enqueue_script('bootstrap.js', WP_PLUGIN_URL. '/dc_bike_tracking_back/js/bootstrap.min.js' );
    wp_enqueue_script('jquery-ui.js', WP_PLUGIN_URL. '/dc_bike_tracking_back/js/jquery-ui.js' );
	// wp_enqueue_script('script.js?time='.date("YmdHis"), WP_PLUGIN_URL. '/dc_bike_tracking_back/js/script.js' );
    wp_enqueue_script('scriptback.js?time='.date("YmdHis"), WP_PLUGIN_URL. '/dc_bike_tracking_back/js/scriptback.js' );
    wp_enqueue_style('thickbox');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_media();  
}

add_action( 'admin_enqueue_scripts', 'dc_bike_tracking_back_js_include' );

add_action( 'admin_menu', 'dc_bike_tracking_back_custom_admin_menu' );
 
function dc_bike_tracking_back_custom_admin_menu() {
    add_options_page(
        'Tracking - Servicio',
        'Tracking - Servicio',
        'manage_options',
        'dc-bike-tracking-back',
        'dc_bike_tracking_back_main_page'
    );
}

function dc_bike_tracking_back_main_page() {
	define('dc_bike_tracking_back_PLUGUIN_NAME','dc_bike_tracking_back');
	include_once dc_bike_tracking_back_get_url() .'view/main_page.php';    
}

register_activation_hook( __FILE__, 'dc_bike_tracking_back_create_db' );
function dc_bike_tracking_back_create_db() {
    
}

function dc_bike_tracking_back_get_all()
{ 
    //return get_banners();
}  
add_action('init', 'dc_bike_tracking_back_get_all');



?>