<?php 

function exportar_excel($vehicles, $campaigns, $models){


	$objPHPExcel = new PHPExcel();
	   
   //Informacion del excel
   $objPHPExcel->
    getProperties()
        ->setCreator("magadalenamedio.com")
        ->setLastModifiedBy("magadalenamedio.com")
        ->setTitle("Motorysa - Respaldo")
        ->setSubject("Motorysa - Respaldo")
        ->setDescription("Motorysa - Respaldo")
        ->setKeywords("Motorysa - Respaldo")
        ->setCategory("Motorysa - Respaldo");    
	   $sheet = 0;
	   $i = 1;  
	   $objPHPExcel->setActiveSheetIndex($sheet)->setTitle("VINS");
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('A'.$i, 'VIN');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('B'.$i, 'CAMPAÑA');

	   foreach($vehicles as $vehicle){
	      $i++; 
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('A'.$i, $vehicle->vin);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('B'.$i, $vehicle->campaign);
	      
	      /*
	      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$i, $row["contact_distributor"]);
	      $referidosText = '';
	      $referidos = json_decode($row["contact_referidos"], true);
	      $lfcr =  chr(13);
	        foreach ($referidos as $k=>$v):
	            foreach ($v as $kk=>$vv):
	                $referidosText .= $kk. ': ' . $vv.$lfcr;
	            endforeach;
	        endforeach;

	     $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, $referidosText);
	     $objPHPExcel->setActiveSheetIndex(0)->getStyle('J'.$i)->getAlignment()->setWrapText(true);*/
	   }

	   $sheet = 1;
	   $i = 1;  
	   $objWorkSheet = $objPHPExcel->createSheet($sheet);
	   $objPHPExcel->setActiveSheetIndex($sheet)->setTitle("CAMPAÑAS");
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('A'.$i, 'CODIGO');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('B'.$i, 'MODELO');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('C'.$i, 'NOMBRE');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('D'.$i, 'MODELOS');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('E'.$i, 'DESCRIPCION');

	   foreach($campaigns as $campaign){
	      $i++; 
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('A'.$i, $campaign->codigo);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('B'.$i, $campaign->modelo);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('C'.$i, $campaign->nombre);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('D'.$i, $campaign->modelos);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('E'.$i, $campaign->descripcion);
	      
	      /*
	      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$i, $row["contact_distributor"]);
	      $referidosText = '';
	      $referidos = json_decode($row["contact_referidos"], true);
	      $lfcr =  chr(13);
	        foreach ($referidos as $k=>$v):
	            foreach ($v as $kk=>$vv):
	                $referidosText .= $kk. ': ' . $vv.$lfcr;
	            endforeach;
	        endforeach;

	     $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, $referidosText);
	     $objPHPExcel->setActiveSheetIndex(0)->getStyle('J'.$i)->getAlignment()->setWrapText(true);*/
	   }

	   $sheet = 2;
	   $i = 1;  
	   $objWorkSheet = $objPHPExcel->createSheet($sheet);
	   $objPHPExcel->setActiveSheetIndex($sheet)->setTitle("MODELOS");
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('A'.$i, 'CODIGO');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('B'.$i, 'MODELO');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('C'.$i, 'IMAGEN_PERFIL');
	   $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('D'.$i, 'IMAGEN_ICONO');

	   foreach($models as $model){
	      $i++; 
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('A'.$i, $model->codigo);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('B'.$i, $model->modelo);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('C'.$i, $model->imagen_perfil);
	      $objPHPExcel->setActiveSheetIndex($sheet)->setCellValue('D'.$i, $model->imagen_icono);
	      
	      /*
	      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$i, $row["contact_distributor"]);
	      $referidosText = '';
	      $referidos = json_decode($row["contact_referidos"], true);
	      $lfcr =  chr(13);
	        foreach ($referidos as $k=>$v):
	            foreach ($v as $kk=>$vv):
	                $referidosText .= $kk. ': ' . $vv.$lfcr;
	            endforeach;
	        endforeach;

	     $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, $referidosText);
	     $objPHPExcel->setActiveSheetIndex(0)->getStyle('J'.$i)->getAlignment()->setWrapText(true);*/
	   }
	 	
	 	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
		//$objWriter->save('php://output');
		$file = md5(date("YmdHis")).'.xlsx';
	 	$objWriter->save('../temp/'. $file);
	 	return $file;
}

function cargar_desde_excel(&$vehicles, &$campaigns, &$models,$archivo){

	$objPHPExcel = PHPExcel_IOFactory::load($archivo);
	$objPHPExcel->setActiveSheetIndex(0);
	//Obtengo el numero de filas del archivo
	$numRows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
	$informacion = array();
	for ($i = 2; $i <= $numRows; $i++) {
	    $vehicles[] = array(
	        'vin' => $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue(),
	        'campaign' => $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue()
	    );
	 }

	$objPHPExcel->setActiveSheetIndex(1);
	//Obtengo el numero de filas del archivo
	$numRows = $objPHPExcel->setActiveSheetIndex(1)->getHighestRow();
	$informacion = array();
	for ($i = 2; $i <= $numRows; $i++) {
	    $campaigns[] = array(
	        'codigo' => $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue(),
	        'modelo' => $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue(),
	        'nombre' => $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue(),
	        'modelos' => $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getCalculatedValue(),
	        'descripcion' => $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getCalculatedValue()
	    );
	 }

	$objPHPExcel->setActiveSheetIndex(2);
	//Obtengo el numero de filas del archivo
	$numRows = $objPHPExcel->setActiveSheetIndex(2)->getHighestRow();
	$informacion = array();
	for ($i = 2; $i <= $numRows; $i++) {
	    $models[] = array(
	        'codigo' => $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue(),
	        'modelo' => $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue(),
	        'imagen_perfil' => $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue(),
	        'imagen_icono' => $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getCalculatedValue()
	    );
	 }
}

?>