<?php 

header('content-type: application/json; charset=utf-8');


require_once '../lib/excel/PHPExcel.php';
require_once("../lib/excel/PHPExcel/IOFactory.php");

require_once '../model/model.php';
require_once '../controller/exportar.php';

if(!isset($_POST['action'])){
	$resultado = array('status'=>'ERROR', 'message' =>'ha ocurrido un error');
	echo json_encode($resultado);
	exit();
}

$action = $_POST['action'];

switch ($action) {
	case 'cargando':
		
		$upload_folder ='../temp';
		$nombre_archivo = $_FILES['archivo']['name'];
		$tipo_archivo = $_FILES['archivo']['type'];
		$tamano_archivo = $_FILES['archivo']['size'];
		$tmp_archivo = $_FILES['archivo']['tmp_name'];
		$ext	= pathinfo($nombre_archivo, PATHINFO_EXTENSION);
		$archivador = $upload_folder . '/' . md5(date("YmdHis")) . '.' . $ext;;
		
		if (!move_uploaded_file($tmp_archivo, $archivador)) {
			$return = array('status'=>'ERROR', 'message' =>'ha ocurrido un error al cargar el archivo');
			echo json_encode($return);
			exit();
		}
		/*
		$objPHPExcel = PHPExcel_IOFactory::load($archivador);
		$objPHPExcel->setActiveSheetIndex(0);
		//Obtengo el numero de filas del archivo
		$numRows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
		$informacion = array();
		for ($i = 1; $i <= $numRows; $i++) {
		    $informacion[] = array(
		        'nombre' => $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue(),
		    );
		 }*/
		 cargar_desde_excel($vehicles, $campaigns, $models, $archivador);

		 //error_log(json_encode($vehicles));
		 //error_log(json_encode($campaigns));
		 //error_log(json_encode($models));

		 $respaldo = new Respaldo();
		 $log = array();
		 $cont = 1;
		 $respaldo->delete_all_vehicles();
		 foreach ($vehicles as $vehicle) {
		 	$reg = $respaldo->insert_vehicle($vehicle, $cont);
		 	if($reg['status'] != 'OK'){
		 		$log[] = $reg;
		 	}
		 	$cont++;
		 }
		 
		 $log2 = array();
		 $cont = 1;
		 $respaldo->delete_all_campaigns();
		 foreach ($campaigns as $campaign) {
		 	$reg = $respaldo->insert_campaign($campaign, $cont);
		 	if($reg['status'] != 'OK'){
		 		$log2[] = $reg;
		 	}
		 	$cont++;
		 }

		 $log3 = array();
		 $cont = 1;
		 $respaldo->delete_all_models();
		 foreach ($models as $model) {
		 	$reg = $respaldo->insert_model($model, $cont);
		 	if($reg['status'] != 'OK'){
		 		$log3[] = $reg;
		 	}
		 	$cont++;
		 }

		 echo json_encode(array('status'=>'OK', 'message'=>'carga finalizada', 'log_vehiculos'=>$log, 'log_campaigns' => $log2, 'log_models' => $log3));
		 
		break;
	
	case 'descargando':
		$respaldo = new Respaldo();
		$file = exportar_excel($respaldo->get_all_vehicles(), $respaldo->get_all_campaigns(), $respaldo->get_all_models());		
		echo json_encode(array('status'=>'OK', 'archivo' => $file));
		break;

	default:
		# code...
		break;
}


?>