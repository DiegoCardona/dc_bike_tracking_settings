<?php 

header('Access-Control-Allow-Origin: *');
header('content-type: application/json; charset=utf-8');


// require_once '../lib/excel/PHPExcel.php';
// require_once("../lib/excel/PHPExcel/IOFactory.php");

require_once '../model/model.php';
// require_once '../controller/exportar.php';

if(!isset($_POST['action']) && !isset($_GET['action'])){
	$resultado = array('status'=>'ERROR', 'message' =>'ha ocurrido un error');
	//echo json_encode($resultado);
	exit();
}

if(isset($_POST['action'])){
	$action = $_POST['action'];	
}else{
	$action = $_GET['action'];
}

function get_image_url_v3($image){
	if($image)
		if(count($image) > 0){
			return $image[0];
		}
	return '';
}

function sz_banner_url($url, $url_gral){
	$new_url = $url;
	if(trim($url) == ''){
		$new_url = $url_gral;
	}
	return $new_url;
}

switch ($action) {
	case 'getBikes':
			$name  = $_POST['name'];
			$email = $_POST['email'];
			$tracking = new Tracking();
			$bikes = $tracking->getBikes($name, $email);
			
			$parameters = $tracking->get_all_parameters();

			$parametersList = [];
			foreach ($parameters as $parameter) {
				$parametersList[$parameter->key] = $parameter->value;
			}

			$result = array('status' => 'OK','bikes' => $bikes, 'parameters' => $parametersList, 'plugin_url' => WP_PLUGIN_URL);
			echo json_encode($result);
		break;

	case 'getBike':
			
			$id = $_POST['id'];
			$tracking = new Tracking();
			$bike = $tracking->getBike($id);
			
			$result = array('status' => 'OK','bike' => $bike[0]);
			echo json_encode($result);
		break;

	case 'getBikeAndServices':
			
			$id = $_POST['id'];
			$tracking = new Tracking();
			$bike = $tracking->getBike($id);
			$services = $tracking->getBikeServices($id);
			
			$result = array('status' => 'OK','bike' => $bike[0],'services' => $services, 'plugin_url' => WP_PLUGIN_URL);
			echo json_encode($result);
		break;

	case 'getService':
			
			$id = $_POST['id'];
			$tracking = new Tracking();
			$service = $tracking->getService($id);
			
			$result = array('status' => 'OK','service' => $service[0], 'plugin_url' => WP_PLUGIN_URL);
			echo json_encode($result);
		break;

	case 'deleteService':
			
			$id = $_POST['id'];
			$tracking = new Tracking();
			$result = $tracking->deleteService($id);			
			echo json_encode($result);
		break;

	case 'newService':
			
			$id 				= $_POST['id'];
			$fecha 				= $_POST['fecha'];
			$bike_id 			= $_POST['bike_id'];
			$clase				= $_POST['clase'];
			$observaciones		= $_POST['observaciones'];
			$fotos_id 			= $_POST['fotos_id'];
			$fotos_url 			= $_POST['fotos_url'];
			
			$tracking = new Tracking();
			if($id == null || $id == ''){
				$result = $tracking->insert_service($fecha, $clase, $observaciones, $fotos_id, $fotos_url, $bike_id);
				$result['bike_id'] = $bike_id;	
			}else{
				$result = $tracking->update_service($id, $fecha, $clase, $observaciones, $fotos_id, $fotos_url, $bike_id);
				$result['bike_id'] = $bike_id;	
			}
			

			echo json_encode($result);
		break;
	
	case 'getPicture':
		$idPicture = $_POST['foto'];

		
			$url = get_image_url_v3(wp_get_attachment_image_src($idPicture,'large'));
			
			echo json_encode($url);
		
		break;
	
	default:
		# code...
		break;
}


?>