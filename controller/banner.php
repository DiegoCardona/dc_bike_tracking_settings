<?php

require_once(dirname( __FILE__ ).'/../../../../wp-config.php');
require_once(dirname( __FILE__ ).'/../../../../wp-load.php');

//require_once '../model/bannerschedule.php';
global $wpdb;
function get_image_url_v3($image){
	if($image)
		if(count($image) > 0){
			return $image[0];
		}
	return '';
}

function sz_banner_url($url, $url_gral){
	$new_url = $url;
	if(trim($url) == ''){
		$new_url = $url_gral;
	}
	return $new_url;
}

function get_banners(){

	$banner = new Banner_schedule();
	$banner =  $banner->get_current();
	$res = array();
	/*
	"banner_encabezado":"","banner_pie":"","banner_lateral_izq":"","banner_lateral_der":"","banner_menu_sup":"","banner_menu_inf":"","banner_mobile_sup":"","banner_mobile_inf":
	*/
			//sz_banner_url($banner->,$banner->url)
			//consultamos la imagenes originales de wordpress
	$key = array("@url@", "@banner@", "@name@");

	if($banner->banner_encabezado != ""){
		$banner->banner_encabezado_url = get_image_url_v3(wp_get_attachment_image_src($banner->banner_encabezado,'large'));
		$fichero = file_get_contents(dirname( __FILE__ ).'/../view/banner_encabezado.php', true);
		$value   = array(sz_banner_url($banner->url_banner_encabezado,$banner->url), $banner->banner_encabezado_url, $banner->name);
		$fichero = str_replace($key, $value, $fichero);
		$res['banner_encabezado'] = $fichero;
	}
	if($banner->banner_lateral_izq != ""){
		$banner->banner_lateral_izq_url = get_image_url_v3(wp_get_attachment_image_src($banner->banner_lateral_izq,'large'));
		$fichero = file_get_contents(dirname( __FILE__ ).'/../view/banner_lateral_izq.php', true);
		$value   = array(sz_banner_url($banner->url_banner_lateral_izq,$banner->url), $banner->banner_lateral_izq_url, $banner->name);
		$fichero = str_replace($key, $value, $fichero);
		$res['banner_lateral_izq'] = $fichero;
	}
	if($banner->banner_pie != ""){
		$banner->banner_pie_url = get_image_url_v3(wp_get_attachment_image_src($banner->banner_pie,'large'));
		$fichero = file_get_contents(dirname( __FILE__ ).'/../view/banner_pie.php', true);
		$value   = array(sz_banner_url($banner->url_banner_pie,$banner->url), $banner->banner_pie_url, $banner->name);
		$fichero = str_replace($key, $value, $fichero);
		$res['banner_pie'] = $fichero;
	}
	if($banner->banner_lateral_der != ""){
		$banner->banner_lateral_der_url = get_image_url_v3(wp_get_attachment_image_src($banner->banner_lateral_der,'large'));
		$fichero = file_get_contents(dirname( __FILE__ ).'/../view/banner_lateral_der.php', true);
		$value   = array(sz_banner_url($banner->url_banner_lateral_der,$banner->url), $banner->banner_lateral_der_url, $banner->name);
		$fichero = str_replace($key, $value, $fichero);
		$res['banner_lateral_der'] = $fichero;
	}
	if($banner->banner_menu_sup != ""){
		$banner->banner_menu_sup_url = get_image_url_v3(wp_get_attachment_image_src($banner->banner_menu_sup,'large'));
		$fichero = file_get_contents(dirname( __FILE__ ).'/../view/banner_menu_sup.php', true);
		$value   = array(sz_banner_url($banner->url_banner_menu_sup,$banner->url), $banner->banner_menu_sup_url, $banner->name);
		$fichero = str_replace($key, $value, $fichero);
		$res['banner_menu_sup'] = $fichero;
	}
	if($banner->banner_menu_inf != ""){
		$banner->banner_menu_inf_url = get_image_url_v3(wp_get_attachment_image_src($banner->banner_menu_inf,'large'));
		$fichero = file_get_contents(dirname( __FILE__ ).'/../view/banner_menu_inf.php', true);
		$value   = array(sz_banner_url($banner->url_banner_menu_inf,$banner->url), $banner->banner_menu_inf_url, $banner->name);
		$fichero = str_replace($key, $value, $fichero);
		$res['banner_menu_inf'] = $fichero;
	}
	if($banner->banner_mobile_sup != ""){
		$banner->banner_mobile_sup_url = get_image_url_v3(wp_get_attachment_image_src($banner->banner_mobile_sup,'large'));
		$fichero = file_get_contents(dirname( __FILE__ ).'/../view/banner_mobile_sup.php', true);
		$value   = array(sz_banner_url($banner->url_banner_mobile_sup,$banner->url), $banner->banner_mobile_sup_url, $banner->name);
		$fichero = str_replace($key, $value, $fichero);
		$res['banner_mobile_sup'] = $fichero;
	}
	if($banner->banner_mobile_inf != ""){
		$banner->banner_mobile_inf_url = get_image_url_v3(wp_get_attachment_image_src($banner->banner_mobile_inf,'large'));
		$fichero = file_get_contents(dirname( __FILE__ ).'/../view/banner_mobile_inf.php', true);
		$value   = array(sz_banner_url($banner->url_banner_mobile_inf,$banner->url), $banner->banner_mobile_inf_url, $banner->name);
		$fichero = str_replace($key, $value, $fichero);
		$res['banner_mobile_inf'] = $fichero;
	}
	/*
	
	$banner->banner_mobile_inf_url = get_image_url_v2(wp_get_attachment_image_src($banner->banner_mobile_inf,'large'));
	$res['data'] = $banner;*/

	//$fichero = file_get_contents(dirname( __FILE__ ).'/../view/banner_encabezado.php', true);
	return $res;
}

?>