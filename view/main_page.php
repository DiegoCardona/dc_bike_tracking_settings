<div class="container-md">
    <div class="row">
        <br>
    </div>
    <div class="row">
        <div class="col dc_tracking_col_table">
            <br>
            <form>
              <div class="form-row align-items-center">
                <div class="col-auto">
                  <label class="sr-only" for="inlineFormInput">Nombre</label>
                  <input type="text" class="form-control mb-2" id="dc_tracking_back_name" placeholder="Nombre">
                </div>
                <div class="col-auto">
                  <label class="sr-only" for="inlineFormInputGroup">Correo Electrónico</label>
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">@</div>
                    </div>
                    <input type="text" class="form-control" id="dc_tracking_back_email" placeholder="Correo Electrónico">
                  </div>
                </div>
               
                <div class="col-auto">
                  <button type="button" class="btn btn-primary mb-2 dc_tracking_back_search">Buscar</button>
                </div>
              </div>
            </form>
            <div class="alert alert-success dc_tracking_no_results" role="alert" style="display:none">
              No hay bicicletas registradas aún.
            </div>
            <table class="table dc_tracking_si_results" >
              <thead>
                <tr>
                  <th scope="col" class="dc_tracking_table_desk_col">#</th>
                  <th scope="col" class="">Dueño</th>
                  <th scope="col" class="dc_tracking_table_desk_col">Marca</th>
                  <th scope="col" class="dc_tracking_table_desk_col">Modalidad</th>
                  <th scope="col" class="dc_tracking_table_desk_col">Número De Serie</th>
                  <th scope="col" class="dc_tracking_table_mob_col">Marca-Serie</th>
                  <th scope="col">Acción</th>
                </tr>
              </thead>
              <tbody class="dc_results">
                              
              </tbody>
            </table>
        </div>
    </div>




    <!-- Modal - View -->
    <div class="modal fade dc_modal_bike_view" id="dcModalBikeView" tabindex="-1" aria-labelledby="dcModalBikeViewLabel" aria-hidden="true">
      <input type="hidden" id="dc_tracking_current_bike" value="" />
      <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title dc_modal_title" id="dcModalBikeViewLabel">Bicicleta</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <ul class="nav nav-tabs">
              <li class="nav-item">
                <a class="nav-link dc_tracking_nav_active dc_tracking_view_nav_bike" href="#">Bicicleta</a>
              </li>
              <li class="nav-item">
                <a class="nav-link dc_tracking_view_nav_service" href="#">Servicios</a>
              </li>
              <li class="nav-item">
                <a class="nav-link dc_tracking_view_nav_service_new" href="#">Agregar Servicio</a>
              </li>
            </ul>
             <form class="dc_tracking_view_bike">
              <br>
            
               <div class="divTable" style="border: 1px solid #ddd;" >
                  <div class="divTableBody">
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Número de serie</div>
                          <div class="divTableCell dc_cotizador_type">
                            <span id="dc_tracking_serie_view"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Marca</div>
                          <div class="divTableCell dc_cotizador_from">
                            <span id="dc_tracking_marca_view"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Modalidad</div>
                          <div class="divTableCell dc_cotizador_to">
                            <span id="dc_tracking_modalidad_view"></span>
                          </div>
                      </div>
                      <div class="divTableRow dc_row_peso_table">
                          <div class="divTableCell divTableCell30">Tamaño del Rin</div>
                          <div class="divTableCell dc_cotizador_kg">
                            <span id="dc_tracking_rin_view"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Material de fabricación</div>
                          <div class="divTableCell dc_cotizador_precio">
                            <span id="dc_tracking_material_view"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Grupo de cambios</div>
                          <div class="divTableCell dc_cotizador_seguro">
                            <span id="dc_tracking_grupo_view"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Frenos</div>
                          <div class="divTableCell dc_cotizador_total">
                            <span id="dc_tracking_frenos_view"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Suspensión</div>
                          <div class="divTableCell dc_cotizador_total">
                            <span id="dc_tracking_suspension_view"></span>
                          </div>
                      </div>
                  </div>
              </div>

              
            </form>
            <form class="dc_tracking_view_service">
              <div class="row">
                <div class="col">
                   <div class="alert alert-success dc_tracking_no_results_services" role="alert" style="display:none">
                    No hay servicios registrados.
                  </div>
                  <table class="table dc_tracking_si_results_services" >
                    <thead>
                      <tr>
                        <th scope="col" class="">#</th>
                        <th scope="col" class="">Fecha</th>
                        <th scope="col" class="">Clase de Servicio</th>
                        <th scope="col" class="">Observación</th>
                        <th scope="col" class="">Accion</th>
                      </tr>
                    </thead>
                    <tbody class="dc_results_services">
                                    
                    </tbody>
                  </table>
                </div>
                
              </div>
            </form>
            <form class="dc_tracking_view_service_new">
              <br>
              <div class="row">
                <div class="col-md-3">
                  <span>Fecha</span>
                </div>
                <div class="col-md-9">
                  <input type="text"  id="dc_tracking_service_fecha">
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-3">
                  <span>Clase de servicio</span>
                </div>
                <div class="col-md-9">
                  <select id="dc_tracking_service_clase"></select>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-3">
                  <span>Observaciones adicionales</span>
                </div>
                <div class="col-md-9">
                  <textarea id="dc_tracking_service_observaciones" maxlength="1000"  rows="4" cols="50"></textarea>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3">
                  <span>Foto(s)</span>
                </div>
                <div class="col-md-9">
                  <div class="custom-file">
                    <input type="button" id="dc_tracking_back_image_btn" 
                    class="btn btn-primary btn-sm custom_media_upload"  value="Adjuntar">
                    <input type="button" value="Limpiar" class="btn btn-danger btn-sm modal_schedule_banner_clear" 
                    data-id="dc_tracking_back_image_" style="display: none">
                    <input type="hidden" id="dc_tracking_back_image_hdn" value="">
                  </div>
                </div>
              </div>
             
              <div class="row dc_tracking_fotos">
                
              </div>
            </form>

            <form class="dc_tracking_view_service_view" style="display: none">
              <br>
              
              <div class="divTable" style="border: 1px solid #ddd;" >
                  <div class="divTableBody">
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Fecha</div>
                          <div class="divTableCell dc_cotizador_type">
                            <span id="dc_tracking_service_view_fecha"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Clase de servicio</div>
                          <div class="divTableCell dc_cotizador_from">
                            <span id="dc_tracking_service_view_clase"></span>
                          </div>
                      </div>
                      <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Observaciones adicionales</div>
                          <div class="divTableCell dc_cotizador_to">
                            <span id="dc_tracking_service_view_observaciones"></span>
                          </div>
                      </div>
                       <div class="divTableRow">
                          <div class="divTableCell divTableCell30">Foto(s)</div>
                          <div class="divTableCell dc_cotizador_to">
                            <button type="button" class="btn btn-primary dc_tracking_slider_fotos" >Ver</button>
                          </div>
                      </div>
                  </div>
              </div>
            
            </form>

            <div class="row dc_tracking_slider" style="display:none;margin-right: 15px;margin-left: 15px;">
                <div class="col">
                    <div class="row">
                      <div class="col">
                        <div class="your-class">
                          <ul class="dc_tracking_slider_elements" >
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="row">  
                      <div class="col" style="text-align:center;">
                        <button type="button" class="btn btn-secondary dc_car_ant"> < Anterior</button>
                        <button type="button" class="btn btn-secondary dc_car_next"> Siguiente > </button>
                      </div>
                    </div>
                </div>
              
            </div> 


            

          </div>
          <div class="modal-footer">
            <input type="hidden" id="dc_tracking_bike_id" value="">
            <input type="hidden" id="dc_tracking_bike_service" value="">
            <button type="button" class="btn btn-primary dc_tracking_service_back" style="display: none">Regresar</button>
            <button type="button" class="btn btn-success dc_tracking_save_service">Guardar Cambios</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>
    
</div>